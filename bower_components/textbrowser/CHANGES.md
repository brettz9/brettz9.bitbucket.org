# 0.2.0

-   BREAKING: Change i18n in metadata, files, site, etc.

# 0.1.0

-   Initial version
