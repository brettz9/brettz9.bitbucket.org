# 0.2.0

-   BREAKING: Follow i18n changes of `textbrowser` in
    metadata, files, site, etc.

# 0.1.0

-   Initial version
